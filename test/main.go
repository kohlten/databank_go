package main

import (
	"fmt"
	"gitlab.com/kohlten/databank"
	"io/ioutil"
	"os"
)

func main() {
	databank, err := databank_go.DatabankInit("archive")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	defer databank.Free()
	varType, data, err := databank.Load("shield.png")
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	if varType != databank_go.DatabankImage {
		fmt.Println("Data is not an image")
		os.Exit(1)
	}
	err = ioutil.WriteFile("shield.png", data, os.ModePerm)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
