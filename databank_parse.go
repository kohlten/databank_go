package databank_go

//#cgo pkg-config: databank squash-0.8
//#include <stdlib.h>
//#include "databank_parse.h"
import "C"
import (
	"errors"
	"unsafe"
)

type Databank C.t_databank
type DatabankStatus C.DatabankStatus
type DatabankType C.DatabankType

const (
	DatabankHashTable        = C.DATABANK_HASHTABLE
	DatbankHashTableNotFound = C.DATABANK_HASHTABLE_NOT_FOUND
	DatabankMalloc           = C.DATABANK_MALLOC
	DatabankInvalidArchive   = C.DATABANK_INVALID_ARCHIVE
	DatbankOptions           = C.DATABANK_OPTIONS
	DatabankIO               = C.DATABANK_IO
	DatabankInvalidHeader    = C.DATABANK_INVALID_HEADER
	DatabankCompressError    = C.DATABANK_COMPRESS_ERROR
	DatabankOk               = C.DATABANK_OK

	DatabankImage = C.DATABANK_IMAGE
	DatabankFont  = C.DATABANK_FONT
	DatabankOther = C.DATABANK_OTHER
)

func DatabankInit(archiveName string) (*Databank, error) {
	cArchiveName := C.CString(archiveName)
	defer C.free(unsafe.Pointer(cArchiveName))
	databank := new(Databank)
	status := C.databank_init((*C.t_databank)(databank), cArchiveName, nil, nil)
	if status != DatabankOk {
		message := C.get_databank_status_message(status)
		goMessage := C.GoString(message)
		return nil, errors.New(goMessage)
	}
	return databank, nil
}

func (databank *Databank) Load(name string) (DatabankType, []byte, error) {
	var status C.DatabankStatus
	var varType C.DatabankType
	cName := C.CString(name)
	defer C.free(unsafe.Pointer(cName))
	length := C.ulong(0)
	cData := C.databank_load((*C.t_databank)(databank), cName, &length, &varType, &status)
	if status != DatabankOk {
		message := C.get_databank_status_message(status)
		goMessage := C.GoString(message)
		return DatabankType(varType), nil, errors.New(goMessage)
	}
	goData := C.GoBytes(cData, C.int(length))
	C.free(cData)
	return DatabankType(varType), goData, nil
}

func (databank *Databank) Free() {
	C.databank_free((*C.t_databank)(databank))
}
